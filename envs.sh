#!/usr/bin/env bash

cat /proc/$(cat /home/vcap/run.pid)/environ | perl -lne 's/([A-Z_]+)=/\n\1=/g; print $_' > /home/vcap/tmp/env.log
